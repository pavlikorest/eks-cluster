resource "aws_security_group" "eks_cluster_node_group" {
  name        = "EKSClusterNodeGroupSecurityGroup"
  description = "Allow TLS inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {

    description = "Allow incoming SSH traffic"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {

    description = "Allow incoming http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {

    description = "Allow incoming https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
data "aws_vpc" "default" {
  default = "true"
}

resource "aws_security_group" "secret2" {
  name        = "SSH for kube"
  description = "first test"

ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_key_pair" "deploy3" {
  key_name   = "deployer-key3"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDd0Rml1tt4N3AQejAfrgMNBeyjO0vpcBb+SqDOVvWEoG3sIFfndsKxg5YMbE0XKwgFd331j87QEW53N/VxHRrqKWc0/8pyQKchxo4hIaD2+qg6W4cZPrSJJbyglJ4kPKTqN/gbd1X2vKpKrTriDyxigzn+9+T1Fl59mNqpmV7Mp319kvMo1uJUHSKU5QnGzGNIHf+oCjRNoc+TmaaXuspTn9Nq/x+GB5MmkzcAA8YawD70nDMr3Shft5Dlr/5zZVDBUnFT7qMDpa2PXavxM/8laJFWOgws0C4SvmcMpelY3uV4jhNAYAQd/XkiGnIRV8TtQiN5NZhWEgeDm+U73wna04dDTyRhlw5PvGywX1HVwlXXMm3C9JjdOIlsHbLxvkMx+u8TEeD5/FRB8QGOw/gAn4+yiEofhXtgICz1zChOqhw58Lure/x92GvVeY3jE3Hvmj0bd95En0A3MrRXe/Uw7M5LyfrwnbsxwBf2Zlacxfarv6VtAaAHKvqPoT8fcBU= orest@orest-VivoBook-15-ASUS-Laptop-X542UF"
}
