#!/bin/bash
sudo apt-get update -y
sudo mkdir /home/ubuntu/kubernetes
cd home/ubuntu/kubernetes/
sudo apt install python3-pip -y
sudo pip3 install awscli --upgrade
sudo curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /home/ubuntu/kubernetes
sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo chmod +x /home/ubuntu/kubernetes/kubectl
sudo echo "export PATH=$PATH:/home/ubuntu/kubernetes/" >> /etc/bash.bashrc
