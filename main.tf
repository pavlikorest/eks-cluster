resource "aws_instance" "kube" {
  ami                    = "ami-0b1deee75235aa4bb"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.secret2.id]
  key_name               = aws_key_pair.deploy3 .key_name
  user_data              = file("kubectl.sh")
  tags = {
    Name    = "kubectl"
    Owner   = "ubuntu"
    project = "DCC"
  }
}


resource "aws_eks_node_group" "node" {
  cluster_name              = aws_eks_cluster.aws_eks.name
  node_group_name           = "node_tuto"
  node_role_arn             = aws_iam_role.eks_nodes.arn
  subnet_ids                = ["subnet-b113aefd", "subnet-9ba9daf1"]
  instance_types            = ["t2.small"]
      
  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]
}
