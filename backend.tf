# S3 bucket for terroform states
terraform {
  backend "s3" {
    bucket = "orest"
    key    = "states/terraform.tfstate"
    region = "eu-central-1"
  }
}

# Retrieve state data from S3
data "terraform_remote_state" "state" {
  backend = "s3"
  config = {
    bucket               = "orest"
    key                  = "states/terraform.tfstate"
    region               = "eu-central-1"
  }
}

